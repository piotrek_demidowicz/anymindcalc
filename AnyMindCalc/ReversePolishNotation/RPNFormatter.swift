//
//  RPNFormatter.swift
//  AnyMindCalc
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import Foundation

class RPNFormatter {

    func infixToValue(_ expression: String) -> Double? {
        guard let formatedExpression = format(expression) else { return nil }

        return rpnToValue(formatedExpression)
    }

    func rpnToValue(_ expression: String) -> Double? {
        guard let parsedExpression = parseRpn(expression) else { return nil }

        var stack = Stack<Double>()

        for item in parsedExpression {
            if let number = Double(item) {
                stack.push(number)
            } else {
                guard let oper = Operator(rawValue: item),
                    !oper.isParenthesis else { return nil }

                guard let first = stack.pop(),
                    let second = stack.pop(),
                    let newValue = oper.calculate(second, first) else { return nil }

                stack.push(newValue)
            }
        }

        guard let result = stack.pop(),
            stack.isEmpty else { return nil }

        return result
    }

    func format(_ expression: String) -> String? {
        guard let parsedExpression = parseInfix(expression) else { return nil }

        var stack = Stack<Operator>()

        var result = ""

        for element in parsedExpression {
            if Double(element) != nil { // number
                result += element + " "
            } else {
                if element == "(" {
                    guard let oper = Operator(rawValue: element) else { return nil }

                    stack.push(oper)
                } else if element == ")" {
                    while stack.peek()! == .rightParenthesis {
                        result += (stack.pop()?.rawValue ?? "???")  + " "
                    }
                } else { // +, -, *, /, %
                    guard let oper = Operator(rawValue: element) else { return nil }

                    if oper.priority > stack.peek()?.priority ?? -1 { // if stack is empty to -> -1
                        stack.push(oper)
                    } else {
                        while stack.peek()?.priority ?? -1 >= oper.priority {
                            result += (stack.pop()?.rawValue ?? "???")  + " "
                        }

                        stack.push(oper)
                    }
                }
            }
        }

        while !stack.isEmpty {
            if let elem = stack.pop(), elem != .leftParenthesis, elem != .rightParenthesis {
                result += elem.rawValue  + " "
            }

        }

        return result.trimmingCharacters(in: .whitespaces)
    }

    func parseRpn(_ expression: String) -> [String]? {
        return expression.components(separatedBy: " ")
    }

    func parseInfix(_ expression: String) -> [String]? {
        var result = [String]()
        var currentNumber = ""

        func appendCurrentNumberIfNeeded() {
            if !currentNumber.isEmpty { // move number to result
                result.append(currentNumber)
                currentNumber = "" // reset
            }
        }

        for char in expression.trimmingCharacters(in: .whitespaces) {
            if Int(String(char)) != nil || char == "." { // numbers
                currentNumber.append(char)
            } else { // operators
                appendCurrentNumberIfNeeded()
                result.append(String(char))
            }
        }

        appendCurrentNumberIfNeeded()

        return result
    }
}
