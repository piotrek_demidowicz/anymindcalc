//
//  Operator.swift
//  AnyMindCalc
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import Foundation

enum Operator: String {
    case addition = "+"
    case subtraction = "-"
    case division = "/"
    case multiplication = "*"
    case leftParenthesis = "("
    case rightParenthesis = ")"

    static func valid(_ string: String) -> Bool {
        return Operator(rawValue: string) != nil
    }

    var priority: Int {
        switch self {
        case .leftParenthesis:
            return 0
        case .addition, .subtraction, .rightParenthesis:
            return 1
        case .multiplication, .division:
            return 2
        }
    }

    func calculate(_ leftOp: Double, _ rightOp: Double) -> Double? {
        switch self {
        case .addition:
            return leftOp + rightOp
        case .subtraction:
            return leftOp - rightOp
        case .multiplication:
            return leftOp * rightOp
        case .division:
            return leftOp / rightOp
        default: return nil
        }
    }

    var isParenthesis: Bool {
        return self == .leftParenthesis || self == .rightParenthesis
    }
}
