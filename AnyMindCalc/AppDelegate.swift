//
//  AppDelegate.swift
//  AnyMindCalc
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import UIKit
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
        -> Bool {
            setup()
            return true
    }

    private func setup() {
        setupCocoaLumberjack()
    }

    private func setupCocoaLumberjack() {
        DDLog.add(DDOSLogger.sharedInstance)
    }
}
