//
//  ViewModel.swift
//  AnyMindCalc
//
//  Created by Piotrek Demidowicz on 05/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import Foundation
import RxSwift

class ViewModel {
    let input = Variable<String?>(nil)
    let algorithmResult = Variable<String?>(nil)

    private let bag = DisposeBag()
    private let formatter = RPNFormatter()

    init() {
        bind()
    }

    func bind() {
        input.asObservable()
            .map { [weak self] text in self?.evaluateAlg(text) }
            .map { result in String(result ?? 0) }
            .bind(to: algorithmResult)
            .disposed(by: bag)
    }

    private func evaluateAlg(_ expression: String?) -> Double? {
        guard let expression = expression else { return nil }

        return formatter.infixToValue(expression)
    }
}
