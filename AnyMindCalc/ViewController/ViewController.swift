//
//  ViewController.swift
//  AnyMindCalc
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ViewController: UIViewController {
    @IBOutlet weak var expressionTextView: UITextView!
    @IBOutlet weak var algorithmResultLabel: UILabel!

    private let bag = DisposeBag()
    private let viewModel = ViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
    }

    func bind() {
        expressionTextView.rx.text.orEmpty
            .bind(to: viewModel.input)
            .disposed(by: bag)

        viewModel.algorithmResult
            .asObservable()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .subscribeOn(MainScheduler.instance)
            .bind(to: algorithmResultLabel.rx.text)
            .disposed(by: bag)
    }
}
