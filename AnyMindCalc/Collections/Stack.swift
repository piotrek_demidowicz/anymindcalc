//
//  Stack.swift
//  AnyMindCalc
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import Foundation

struct Stack<T> {
    var list = [T]()

    mutating func push(_ element: T) {
        list.append(element)
    }

    mutating func pop() -> T? {
        guard !isEmpty else { return nil }

        return list.removeLast()
    }

    func peek() -> T? {
        guard !isEmpty else { return nil }

        return list.last
    }

    var isEmpty: Bool {
        return list.isEmpty
    }

    var count: Int {
        return list.count
    }
}
