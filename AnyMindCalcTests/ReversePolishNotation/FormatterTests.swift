//
//  FormatterTests.swift
//  AnyMindCalcTests
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import XCTest
import CocoaLumberjack
@testable import AnyMindCalc

class FormatterTests: XCTestCase {
    let dataParse = [
        "2+2+2":["2","+","2","+","2"],
        "23*12342+1-2":["23","*","12342","+","1","-","2"],
        "23*12342+1-2/(2-1)":["23","*","12342","+","1","-","2","/","(","2","-","1",")"]
    ]
//    let dataParse = [
//        "23*12342+1-2/(2-1)":["23","*","12342","+","1","-","2","/","(","2","-","1",")"]
//    ]

    let dataFormat = [
        "23*12342+1-2":"23 12342 * 1 + 2 -"
    ]

    let dataCalc = [
//        "2+2": 4,
//        "2+2+2": 8,
//        "23*12342+1-2": 283865,
        "23*12342+1-2/(2-1)": 283865
    ]

    var formatter: RPNFormatter?

    override func setUp() {
        formatter = RPNFormatter()
    }


    func testFormat_validExpression_properResult() {
        let expression = "2+2*3"
        let result = formatter?.format(expression)
        let expectedResult = "2 2 3 * +"


        XCTAssertEqual(result, expectedResult, "Format returns unexpected result")
    }


    func testFormat_validExpressions_properResult() {
        let expressions = dataFormat.keys
        let result = Dictionary(uniqueKeysWithValues: expressions.map{ ($0, formatter?.format($0)) })

        XCTAssertEqual(result, dataFormat, "Format failed")
    }

    func testParseInfix_validExpressions_properResult() {
        let expressions = dataParse.keys
        let result = Dictionary(uniqueKeysWithValues: expressions.map{ ($0, formatter?.parseInfix($0)) })

        XCTAssertEqual(result, dataParse, "Parse Infix failed")
    }

    func testRpnToValue_validRPNExpression_properResult() {
        let rpnExpression = "12 2 3 4 * 10 5 / + * +"
        let expectedResult = 40
        let result = formatter?.rpnToValue(rpnExpression)

        XCTAssertEqual(result, expectedResult, "RPN calculation failed")
    }

    func testInfixToValue_validInfixExpressions_properResult() {
        for item in dataCalc {
            let result = formatter?.infixToValue(item.key)
            let expectedResult = item.value
            XCTAssertEqual(result, expectedResult, "asdasdasd")
        }


//        XCTAssertEqual(result, expectedResult, "RPN calculation failed")
    }
}
