//
//  StackTests.swift
//  AnyMindCalcTests
//
//  Created by Piotrek Demidowicz on 02/02/2019.
//  Copyright © 2019 Piotr Demidowicz. All rights reserved.
//

import XCTest
@testable import AnyMindCalc

class StackTests: XCTestCase {

    func testPush_empty_isNotEmpty() {
        var stack = Stack<Int>()
        stack.push(2)

        XCTAssertFalse(stack.isEmpty, "Push test failed")
    }

    func testPop_isNotEmpty_returnValue() {
        var stack = Stack<Int>()
        stack.push(2)
        stack.push(3)

        let element = stack.pop()

        XCTAssertNotNil(element, "Pop test failed. Can not return element")
    }

    func testPeek_isNotEmpty_returnGoodValue() {
        var stack = Stack<Int>()
        stack.push(2)
        stack.push(3)

        let element = stack.peek()

        XCTAssertEqual(element, 3, "Peek test failed. Wrong element")

    }

}
